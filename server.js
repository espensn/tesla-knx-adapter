const tjs = require('teslajs');
const path = require('path');
const fs = require('fs');
const knx = require('knx');
const winston = require('winston');

const log = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({ filename: 'application.log' })
    ]
});

const appDir = path.dirname(require.main.filename);

const configuration = require(appDir + "/configuration/configuration.json");
let teslaConf;

try {
    teslaConf = require(appDir + "/configuration/tesla-credentials.json");
} catch (ex) {
    log.error("No tesla configuration found. Run setup.")
    process.exit()

}

function storeToken(token) {
    teslaConf.token = token;
    teslaConf.password = "";
    fs.writeFile(appDir + "/configuration/tesla-credentials.json",JSON.stringify(teslaConf),function(err){
        if(err) throw err;
    })
}

function storeVehicleId(vehicleID) {
    teslaConf.vehicleID = vehicleID;
    fs.writeFile(appDir + "/configuration/tesla-credentials.json",JSON.stringify(teslaConf),function(err){
        if(err) throw err;
    })
}

function getLoginToken() {
    if (!teslaConf.token) {
        if(!teslaConf.username || !teslaConf.password) {
            return Promise.reject("Please define username and password in tesla-credentials.json");
        }

        log.info("No cached token found. Initiating login");
        return tjs.loginAsync(teslaConf.username, teslaConf.password)
            .then(result => {
                if(result.authToken) {
                    storeToken(result.authToken);
                    log.info("Login successful. Token found:", result.authToken);
                    return result.authToken;
                } else {
                    return Promise.reject("Login failed");
                }
            });
    } else {
        log.info("Token already fetched, using cached token:", teslaConf.token);
        return Promise.resolve(teslaConf.token);
    }
}

function getVehicleId() {
    if (!teslaConf.vehicleID) {
        log.info("No cached vehicle ID found. Fetching vehicle.");
        return tjs.vehiclesAsync({authToken: teslaConf.token})
            .then(vehicle => {
                storeVehicleId(vehicle.id);
                log.info("Vehicle ID found:", teslaConf.vehicleID);
                return teslaConf.vehicleID;
            });
    } else {
        log.info("Vehicle ID already fetched, use cached ID: ", teslaConf.vehicleID);
        return Promise.resolve(teslaConf.vehicleID);
    }
}

//On start: Login to get a token
function getVehicleData() {
    return getLoginToken()
        .then(getVehicleId)
        .then(() => tjs.vehicleDataAsync({authToken: teslaConf.token, vehicleID: teslaConf.vehicleID}))
}

function climateStart() {
    return getLoginToken()
        .then(getVehicleId)
        .then(() => tjs.climateStart({authToken: teslaConf.token, vehicleID: teslaConf.vehicleID}, function (err, result) {
            if (result.result) {
                console.log("\nClimate is now: " + "ON");
            } else {
                console.log(result.reason);
            }
        }))
}

function climateStop() {
    return getLoginToken()
        .then(getVehicleId)
        .then(() => tjs.climateStop({authToken: teslaConf.token, vehicleID: teslaConf.vehicleID}, function (err, result) {
            if (result.result) {
                console.log("\nClimate is now: " + "OFF");
            } else {
                console.log(result.reason);
            }
        }))
}

function setTemp(temperature) {
    return getLoginToken()
        .then(getVehicleId)
        .then(() => tjs.setTemps({authToken: teslaConf.token, vehicleID: teslaConf.vehicleID}, temperature, temperature, function (err, result) {
            if (result.result) {
                console.log("\nTemperature is now set to: " + temperature);
            } else {
                console.log(result.reason);
            }
        }))
}

function startCharge() {
    return getLoginToken()
        .then(getVehicleId)
        .then(() => tjs.startCharge({authToken: teslaConf.token, vehicleID: teslaConf.vehicleID}, function (err, result) {
            if (result.result) {
                console.log("\nCharging started");
            } else {
                console.log(result.reason);
            }
        }))
}

function stopCharge() {
    return getLoginToken()
        .then(getVehicleId)
        .then(() => tjs.stopCharge({authToken: teslaConf.token, vehicleID: teslaConf.vehicleID}, function (err, result) {
            if (result.result) {
                console.log("\nCharging stopped");
            } else {
                console.log(result.reason);
            }
        }))
}

function validateGroupAddressForRead(groupAddress) {
    for(key in configuration.read) {
        if(configuration.read[key].groupAddress === groupAddress) {
            return true;
        }
    }

    return false;
}

function validateGroupAddressForWrite(groupAddress) {
    for(key in configuration.write) {
        if(configuration.write[key].groupAddress === groupAddress) {
            return true;
        }
    }

    return false;
}

function getDataForGroupAddress(groupAddress, vehicleData) {
    var vehicleDataMap = {};
    for (key in vehicleData) {
        if (typeof vehicleData[key] === 'object') {
            for (key2 in vehicleData[key]) {
                vehicleDataMap[key + "." + key2] = vehicleData[key][key2];
            }
        } else {
            vehicleDataMap[key] = vehicleData[key];
        }
    }

    for(key in configuration.read) {
        if(vehicleDataMap.hasOwnProperty(key) && configuration.read[key].groupAddress === groupAddress) {
            log.info(groupAddress + " - " + key + ": " + vehicleDataMap[key] + " (" + configuration.read[key].dpt + ")");
            return Promise.resolve({groupAddress: groupAddress, data: vehicleDataMap[key], dpt: configuration.read[key].dpt})
        }
    }

    return Promise.reject("No data found for groupAddress");
}

function publishDataForGroupAddress(groupAddress) {
    return getVehicleData()
        .then(vehicleData => getDataForGroupAddress(groupAddress, vehicleData))
        .then(res => {
            if(res.data !== null && res.data !== undefined) {
                connection.write(res.groupAddress, res.data, res.dpt);
                log.info("Publishing data to knx group address",res.groupAddress, res.data, res.dpt);
            } else {
                log.error("Failed publishing data to KNX group address", res.groupAddress, "- Unknown value received from Tesla:", res.data);
            }
        })
}

function writeDataForGroupAddress(groupAddress, value) {
    let writeFunction;
    let writeConfiguration;

    for(key in configuration.write) {
        if(configuration.write[key].groupAddress === groupAddress && typeof value === configuration.write[key].type) {
            if(configuration.write[key].hasOwnProperty("onValue")) {
                if(configuration.write[key].onValue === value) {
                    writeFunction = key;
                    writeConfiguration = configuration.write[key];
                }
            } else {
                writeFunction = key;
                writeConfiguration = configuration.write[key];
            }
        }
    }

    if(!writeFunction || !writeConfiguration) {
        log.error("Unable to map groupAddress to function:", groupAddress);
        return;
    }

    if(callableWriteFunctions[writeFunction]) {
        callableWriteFunctions[writeFunction](value);
    } else {
        log.error("Unknown write function", writeFunction);
    }
}

function addKnxListeners() {
    connection.on('GroupValue_Read', function (src, dest) {
        log.info("Got read request for dest", dest);
        if(validateGroupAddressForRead(dest)) {
            log.info("GroupAddress validated:", dest);
            withRetry(configuration.tesla.retryCount, publishDataForGroupAddress, dest);
        } else {
            log.info("GroupAddress invalid:", dest);
        }
    });

    connection.on('GroupValue_Write', function (src, dest, value) {
        // log.info("Got write request for dest", dest, value);
        if(validateGroupAddressForWrite(dest)) {
            log.info("GroupAddress for write validated:", src, dest, value);
            withRetry(configuration.tesla.retryCount, writeDataForGroupAddress, dest, value);
        } else {
            //log.info("GroupAddress invalid:", dest);
        }
    });
}

function withRetry(maxRetries, fn, dest, value) {
    return fn(dest, value).catch(function(err) {
        if (maxRetries <= 0) {
            log.error("Unable to call method within retry limit.");
        } else {
            log.error("Failed calling method. Will retry. Error:", err);
            setTimeout(function() {
                withRetry(maxRetries - 1, fn, dest, value)
            }, configuration.tesla.millisecondsBetweenRetries);
        }
    });
}

const callableWriteFunctions = {
    'climateStart': climateStart,
    'climateStop': climateStop,
    'setTemp': setTemp,
    'startCharge': startCharge,
    'stopCharge': stopCharge
};

let opts = {
    handlers: {
        connected: function() {
            log.info('Connected to knx. Ready to receive requests');
            addKnxListeners();
        },
        event: function (evt, src, dest, value) {
            // log.info("%s **** KNX EVENT: %j, src: %j, dest: %j, value: %j",
            //     new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''),
            //     evt, src, dest, value);
        }
    }
};

if(configuration.knx.ip) {
    opts.ipAddr = configuration.knx.ip;
}

var connection = knx.Connection(opts);
log.info("");
log.info("###############################################################################");
log.info("# tesla-knx-adapter initialized. Waiting for KNX connection to be established #");
log.info("###############################################################################");
log.info("");

// callableWriteFunctions['setTemp'](19);
// callableWriteFunctions['climateStart']();

// writeDataForGroupAddress("1/1/109", false);
// publishDataForGroupAddress("1/1/109")