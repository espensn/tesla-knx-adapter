const tjs = require('teslajs');
const path = require('path');
const fs = require('fs');
const prompt = require('prompt');
const winston = require('winston');

const log = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({ filename: 'application.log' })
    ]
});

const appDir = path.dirname(require.main.filename);

let teslaConf;
try {
    teslaConf = require(appDir + "/configuration/tesla-credentials.json");
    log.info("Existing tesla configuration found. Will be overwritten.")
} catch (ex) {
}

let username;
let password;
let vehicleID;
let token;

const schema = {
    properties: {
        username: {
            required: true
        },
        password: {
            hidden: true,
            required: true
        }
    }
};

function storeCredentials() {
    let credentials = {
        username : username,
        token: token,
        vehicleID: vehicleID
    };
    fs.writeFileSync(appDir + "/configuration/tesla-credentials.json",JSON.stringify(credentials));
    return Promise.resolve();
}

function getLoginToken(username, password) {
        if(!username || !password) {
            return Promise.reject("Please define username and password in tesla-credentials.json");
        }

        log.info("Trying to login to Tesla API");
        return tjs.loginAsync(username, password)
            .then(result => {
                if(result.authToken) {
                    token = result.authToken;
                    log.info("Login successful. Token found:", result.authToken);
                    return result.authToken;
                } else {
                    return Promise.reject("Login failed");
                }
            });
}

function getVehicleId() {
        log.info("Trying to fetch vehicle ID");
        return tjs.vehiclesAsync({authToken: token})
            .then(vehicle => {
                vehicleID = vehicle.id;
                log.info("Vehicle ID found:", vehicleID);
                return vehicleID;
            });
}

function init() {
    prompt.start();

    prompt.get(schema, function (err, result) {
        username = result.username;
        password = result.password;

        getLoginToken(username, password)
            .then(getVehicleId)
            .then(() => storeCredentials())
            .then(() => log.info("Setup complete."))
            .catch(error => log.error("Setup failed. Check that you have internet connection, valid username and valid password. Run setup again. Error message:", error))
    });
}

init();
